package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/example-discord-bot-cloudformation/utils"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
	DG     *discordgo.Session
}

var service *Service

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	service = &Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	discordToken := os.Getenv("DISCORD_TOKEN")

	dg, discErr := discordgo.New("Bot " + discordToken)

	if discErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Error creating Discord session: %s", discErr.Error()), service.Log.LogFatal)
		return
	}

	service.DG = dg

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Error opening connection: %s", openErr.Error()), service.Log.LogFatal)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	service.Log.Log("PROCESS: Bot is now running", service.Log.LogInformation)

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == s.State.User.ID {
		service.Log.Log("BLOCKED: MessageCreate event sent by self", service.Log.LogInformation)
		return
	}

	service.Log.Log(fmt.Sprintf("SUCCESS: Message from user (%s): %s", m.Author.Username, m.Content), service.Log.LogInformation)

	response, err := http.Get(service.Config.RequestPath)

	if err != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Failed to make request with error: %s", err.Error()), service.Log.LogHigh)
		return
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	service.Log.Log(fmt.Sprintf("SUCCESS: Got response body from request: %s", string(body)), service.Log.LogInformation)

	return
}
