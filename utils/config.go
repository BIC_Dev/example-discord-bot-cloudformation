package utils

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	RequestPath string `yaml:"REQUEST_PATH"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(env string) *Config {
	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		log.Fatal("Missing config file at path: " + configFile)
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		log.Fatal("Could not parse config file")
	}

	return &config
}
